import React, {useRef, useState} from 'react';
import {Button, StatusBar, StyleSheet, Text, View} from 'react-native';
import {WebView} from 'react-native-webview';

export default function App() {
  const webViewRef = useRef();
  // let backgroundColor = 'white';

  const [backgroundColor, setBackgroundColor] = useState('white');

  // const run = `
  //   document.body.style.backgroundColor = 'red';
  //   true;
  // `;

  const getInjectableJSMessage = (message: any) => {
    return `
      (function() {
        document.dispatchEvent(new MessageEvent('message', {
          data: ${JSON.stringify(message)}
        }));
      })();
    `;
  };

  const run = getInjectableJSMessage({
    message:
      'log apple door pear table house phone desk tablet pirate boat video',
    triggeredAt: new Date(),
  });

  // // somewhere in your code
  // this.webviewRef.injectJavaScript(
  //   getInjectableJSMessage({
  //     message: 'Triggered event',
  //     triggeredAt: new Date(),
  //   })
  // );

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: backgroundColor,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

  // const uri = 'https://google.com'
  const uri = 'https://shapeshift.com/join';

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Text>Web View</Text>
      <View style={{width: '100%', height: '40%'}}>
        <WebView
          ref={webViewRef}
          source={{uri: uri}}
          onLoad={() => {
            console.log('yup');
          }}
          onMessage={event => {
            const data = JSON.parse(event.nativeEvent.data);
            console.log('Event Object: ', event.nativeEvent.url);
            console.log(data.color);
            setBackgroundColor(data.color);
          }}
          originWhitelist={['https://shapeshift.com']}
        />
      </View>
      <View style={{width: '100%', height: '40%'}}>
        <Text>Native View</Text>
        <Button
          title="Communicate to Web App"
          onPress={() => {
            console.log(webViewRef);
            // webViewRef.current.injectJavaScript(run);

            console.log(run);
            webViewRef.current.injectJavaScript(run);
          }}
        />
        <Text>Bacground Color: {backgroundColor} </Text>
      </View>
    </View>
  );
}
