# Native App Test

This project (and its compainon Web App Test) is a proof of concept testing that the web app and the mobile app can communicate with each other and it will navigate to the phone's web browser if it leaves the specified domain.

Demonstrating communication between WebView and Native App by changing the background of the Native App and Web App to be red - https://drive.google.com/file/d/1hMPf1d0JZffd1G0Q08sR39beFv9N0qr7/view?usp=sharing

Go to non-ShapeShift domain in phone's mobile browser - https://drive.google.com/file/d/1BCeuF8pF6rhdoTG0huzkb8oph6BmJGZR/view?usp=sharing
